package com.tdd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.ImportResource;

/**
 * @author tudedong
 * @description 服务提供者启动类
 * @date 2020-07-13 23:07:07
 */
@SpringBootApplication
@EnableHystrix
@ImportResource("classpath:dubbo-user-service.xml")
public class UserServiceApplication {

    public static void main(String[] args){
        SpringApplication.run(UserServiceApplication.class);
    }
}
