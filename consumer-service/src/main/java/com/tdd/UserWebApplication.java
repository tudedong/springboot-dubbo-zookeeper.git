package com.tdd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.ImportResource;

/**
 * @author tudedong
 * @description
 * @date 2020-07-13 23:19:01
 */
@SpringBootApplication
@EnableHystrix
@ImportResource("classpath:dubbo-consumer-service.xml")
public class UserWebApplication {

    public static void main(String[] args){
        SpringApplication.run(UserWebApplication.class);
    }
}
