package com.tdd.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.tdd.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tudedong
 * @description
 * @date 2020-07-13 23:17:24
 */
@RestController
public class UserController {

    /**
     * 告诉消费者不进行依赖检查
     */
    @Autowired
    private UserService userService;

    @HystrixCommand(fallbackMethod = "sayHelloError")
    @GetMapping("/sayHello")
    public String sayHello(){
        System.out.println("调用了sayHello方法了...");
        return userService.sayHello("xiao xi yuan");
    }

    public String sayHelloError(){
        return "hystrix fallback value...";
    }
}
