package com.tdd.service;

/**
 * @author tudedong
 * @description 公共接口
 * @date 2020-07-13 23:02:57
 */
public interface UserService {

    /**
     * 提供方法
     * @param message
     * @return
     */
    String sayHello(String message);
}
